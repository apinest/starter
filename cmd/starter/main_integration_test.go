//+build integration test to top

package main

import (
	"io"
	"net/http"
	"testing"
)

func TestMain(t *testing.T){

	testSet := getMainTestSet()

	// Run each testcase
	for _, testCase := range testSet {


		res, err := http.Get(testCase.request)

		if err != nil{
			t.Error(err)
		}
		defer res.Body.Close()

		if(res.StatusCode == testCase.expectedStatusCode){
			body, _ := io.ReadAll(res.Body)

			if string(body) != testCase.expectedBody {
				t.Error("Expected body: \"" + testCase.expectedBody +"\" , got: \"" + string(body) + "\"")
			}
		}
	}
}

type MainTestCase struct {
	request        string
	expectedStatusCode int
	expectedBody       string
}

// Create different testcases
func getMainTestSet() []MainTestCase {
	testSet := []MainTestCase{
		{"http://starter:8080/memes/memes?id=602a191b95a1a431ac143bb3", 200, `{"_id":"602a191b95a1a431ac143bb3","name":"Me","caption":"Test","url":"https://imgk.timesnownews.com/story/Won.png?tr=w-1200,h-900"}`},
		{"http://starter:8080/sample", 404, "Cannot find requested resource"},
	}
	return testSet
}
