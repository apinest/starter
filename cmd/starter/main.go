package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/apinest/apinest/nestLogger"
	"gitlab.com/apinest/apinest/nestMiddlewares"
	"gitlab.com/apinest/apinest/nestReverseProxy"
	"gitlab.com/apinest/apinest/nestRouter"
)

const (
	MEMES_SERVER   = "http://memes-service.herokuapp.com"
	HEROES_SERVER  = "http://heroes-service.herokuapp.com"
	THREATS_SERVER = "http://threats-service.herokuapp.com"
)

// Main function Entry point of program
func main() {
	commonMiddlewares := nestMiddlewares.New()
	commonMiddlewares.Add(nestLogger.RequestLogger)

	var router = nestRouter.NewRouter()

	router.Handle("/memes", commonMiddlewares.ThenFunc(nestReverseProxy.HandleRequestAndRedirect(MEMES_SERVER)))
	router.Handle("/heroes", commonMiddlewares.ThenFunc(nestReverseProxy.HandleRequestAndRedirect(HEROES_SERVER)))
	router.Handle("/threats", commonMiddlewares.ThenFunc(nestReverseProxy.HandleRequestAndRedirect(THREATS_SERVER)))

	http.Handle("/", router.Exec())

	PORT := fmt.Sprintf(":%s", os.Getenv("PORT"))
	if len(PORT) == 1 {
		PORT = ":8080"
	}

	nestLogger.Log(fmt.Sprintf("Proxy Server Listening on port %s", PORT))
	if err := http.ListenAndServe(PORT, nil); err != nil {
		panic(err)
	}
}
